# 前言
#####
#安装edusoho，在centos上安装了几次都没有成功。
#官方推荐的教程是在ubuntu，我个人对ubuntu系统不熟悉，我对centos熟悉一些，所以我就在centos上安装edusoho。
#环境：vmware 15 上安装的 centos 7.6
#所有有安装包，都来自各个开源镜像站点
#base epel使用的阿里源
#nginx包来自nginx官方源，nginx版本是1.16.1
#mysql包来自mysql官方源，mysql版本是5.7.27
#php包来自webtatic源，php版本是7.1.32
#主机ip:10.0.0.1  主机上搭建了web站点作为yum源（yum源站点不会的，请自行百度）
#centos的ip:10.0.0.251 hostname:edu
#####

# 0，准备工作
echo "10.0.0.1 yum.cn" >> /etc/hosts

cat > /etc/yum.repos.d/edusoho.repo <<EOF
[edusoho]
name=edusoho
baseurl=http://yum.cn/edusoho_rpms
#mirrorlist=https://mirror.webtatic.com/yum/el7/$basearch/mirrorlist
#failovermethod=priority
enabled=1
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-webtatic-el7
EOF




# 1，安装PHP
yum install php71w php71w-cli php71w-common php71w-fpm php71w-gd php71w-mbstring php71w-intl php71w-mcrypt php71w-mysql php71w-mysqli php71w-pdo php71w-xml php71w-pear php71w-curl php71w-devel php71w-json php71w-opcache php71w-zip php71w-ldap -y

## 1.1 修改php配置文件

#sed -n 's#post_max_size = 8M#post_max_size = 1024M#gp' /etc/php.ini
#sed -n 's#memory_limit = 128M#memory_limit = 1024M#gp' /etc/php.ini
#sed -n 's#upload_max_filesize = 2M#upload_max_filesize = 1024M#gp' /etc/php.ini

sed -i -r '/^post_max_size/s#([0-9]+)M#1024M#g' /etc/php.ini
sed -i -r '/^memory_limit/s#([0-9]+)M#1024M#g' /etc/php.ini
sed -i -r '/^upload_max_filesize/s#([0-9]+)M#1024M#g' /etc/php.ini

egrep '(post_max_size|memory_limit|upload_max_filesize)' /etc/php.ini

#修改文件/etc/php-fpm.d/www.conf
# vi /etc/php-fpm.d/www.conf
# user = nginx
# group = nginx
#user = apache
#group = apache

sed -i -r '/^user/s#apache#nginx#g' /etc/php-fpm.d/www.conf
sed -i -r '/^group/s#apache#nginx#g' /etc/php-fpm.d/www.conf

egrep '(^user|^group)' /etc/php-fpm.d/www.conf

## 1.2 启动php
systemctl start php-fpm.service
systemctl enable php-fpm.service

# 2，安装mysql
yum install mysql mysql-server -y

## 2.1 启动mysql
systemctl start mysqld.service
systemctl enable mysqld.service

## 2.2登陆mysql,修改密码
# grep 'temporary password' /var/log/mysqld.log
# 2019-10-01T19:57:45.874383Z 1 [Note] A temporary password is generated for root@localhost: z5V!MaVdmfok
#################################################
# mysql -uroot -p'z5V!MaVdmfok'

#####################################
mypwd=$(grep 'temporary password' /var/log/mysqld.log| awk '{print $11}')
mysqladmin -uroot -p"$mypwd" password "123456@xY"

# mysql -uroot -p'123456@xY'

#修改密码
# ALTER USER 'root'@'localhost' IDENTIFIED BY '123456@xY';

#创建数据库
# CREATE DATABASE edusoho DEFAULT CHARACTER SET utf8 ;
# GRANT ALL PRIVILEGES ON `edusoho`.* TO 'esuser'@'localhost' IDENTIFIED BY 'Edusoho@123';
# quit;
###########################################
mysql -uroot -p'123456@xY' -e 'CREATE DATABASE edusoho DEFAULT CHARACTER SET utf8 ;'
mysql -uroot -p'123456@xY' -e 'GRANT ALL PRIVILEGES ON `edusoho`.* TO "esuser"@"localhost" IDENTIFIED BY "Edusoho@123";'


# 3，安装nginx
yum install nginx -y

## 3.1 修改nginx默认配置
# vi /etc/nginx/nginx.conf

#在http{}配置中加入：
# client_max_body_size 1024M;
###############################
sed -i "16a  \ \ \ \ client_max_body_size 1024M;" /etc/nginx/nginx.conf


## 3.2 启动nginx
systemctl start nginx.service
systemctl enable nginx.service

## 3.3配置nginx
# vi /etc/nginx/conf.d/edusoho.conf

cat > /etc/nginx/conf.d/edusoho.conf<<EOF
server {

    set \$root_dir /var/www/edusoho;

    # 改成您的网站域名
    server_name www.edu.cn edu.cn;

    root \$root_dir/web;

    error_log /var/log/nginx/edusoho.error.log;
    access_log /var/log/nginx/edusoho.access.log;

    location / {
        index app.php;
        try_files \$uri @rewriteapp;
    }

    location @rewriteapp {
        rewrite ^(.*)$ /app.php/\$1 last;
    }

    location ~ ^/(app|app_dev)\.php(/|\$) {
        fastcgi_pass 127.0.0.1:9000;
        fastcgi_split_path_info ^(.+\.php)(/.*)\$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME \$realpath_root\$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT \$realpath_root;
        fastcgi_param HTTP_X-Sendfile-Type X-Accel-Redirect;
        fastcgi_param HTTP_X-Accel-Mapping /udisk=\$root_dir/app/data/udisk;
        fastcgi_buffer_size 128k;
        fastcgi_buffers 8 128k;
        internal;
    }

    location ~* \.(jpg|jpeg|gif|png|ico|swf)\$ {
        expires 3y;
        access_log off;
        gzip off;
    }

    location ~* \.(css|js)\$ {
        expires 3y;
        access_log off;
    }

    location ~ ^/udisk {
        root \$root_dir/app/data/;
        internal;
    }
    
    # 以下配置允许运行.php的程序，方便于其他第三方系统的集成。
    location ~ \.php\$ {
        # [改] 请根据实际php-fpm运行的方式修改
        fastcgi_pass 127.0.0.1:9000;
        fastcgi_split_path_info ^(.+\.php)(/.*)\$;
        include fastcgi_params;
        fastcgi_param  SCRIPT_FILENAME    \$document_root\$fastcgi_script_name;
        fastcgi_param  HTTPS              off;
        fastcgi_param  HTTP_PROXY         "";
    }
}
EOF

mv /etc/nginx/conf.d/default.conf{,.bak}

## 重启nginx
systemctl restart nginx.service

## 修改目录权限

wget -O /var/www/tools/edusoho-ct-5.1.1.zip http://yum.cn/softs/edusoho-ct-5.1.1.zip

cd /var/www/
unzip edusoho-ct-5.1.1.zip
chown -Rf nginx.nginx /var/www/edusoho

# 下面的方法无法正常使用，视频无法播放(软链接无法正常使用)
# rm -rf /var/www/edusoho/app/data/udisk
# ln -s /mnt/hgfs/EduSohoData  /var/www/edusoho/app/data/udisk

###########################
#在nginx配置文件中，把其中的一行：fastcgi_param HTTP_X-Accel-Mapping /udisk=\$root_dir/app/data/udisk;
#改为：fastcgi_param HTTP_X-Accel-Mapping /udisk=你要的目录路径
#比如我用的是vmware,做了主机文件夹共享，所以我的配置就是：fastcgi_param HTTP_X-Accel-Mapping /udisk=/mnt/hgfs/EduSohoData